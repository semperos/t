# t

```shell
$ t 42
# https://en.wikipedia.org/wiki/42_(number)

$ t 304
# HTTP: 304 - Not Modified
```

## Executable

```shell
$ zig version
# 0.9.0-dev.663+c234d4790

$ zig build-exe -O ReleaseFast --strip --single-threaded --name t src/main.zig
```
