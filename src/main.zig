const std = @import("std");
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;
const t = std.testing;

const c = @cImport({
    @cInclude("gsl/gsl_statistics.h");
    @cInclude("time.h");
});

const ProgramError = error{
    MalformedNumber,
};

/// CLI Usage: t <number>
pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var arena = std.heap.ArenaAllocator.init(gpa.allocator());
    defer arena.deinit();
    var allocator = arena.allocator();

    const answerWriter = std.io.getStdOut().writer();

    try magicNumber(allocator, answerWriter);
}

const Arg = union(enum) {
    integer: i64,
    float: f64,
    integers: ArrayList(i64),
    floats: ArrayList(f64),
    empty: bool,
};

/// `magicNumber` interprets its `arg` as a number or number-like and attempts to give maximally-useful information about it.
fn magicNumber(allocator: Allocator, writer: anytype) !void {
    var answers = ArrayList([]const u8).init(allocator);
    var args = ArrayList([]const u8).init(allocator);
    // CLI args
    var argc: u8 = 0;
    var iter = std.process.ArgIteratorPosix.init();
    while (iter.next()) |arg| : (argc += 1) {
        switch (argc) {
            0 => continue, // executable name
            else => {
                try args.append(arg);
            },
        }
    }
    if (argc == 0) {
        return;
    }
    const normArg = normalizeArg(allocator, args) catch unreachable;
    switch (normArg) {
        .empty => {
            std.log.warn("Couldn't process any of these numbers: {s}\nCheck your params and try again.", .{args});
            std.process.exit(1);
        },
        else => {
            try answerHttp(allocator, &answers, normArg);
            // Date/Time
            // var tmZone = "UTC";
            // var tm: c.tm = c.tm{
            //     .tm_sec = 0,
            //     .tm_min = 0,
            //     .tm_hour = 0,
            //     .tm_mday = 1,
            //     .tm_mon = 0,
            //     .tm_year = 1900, // because this is the root year for time.h
            //     .tm_wday = 0,
            //     .tm_yday = 0,
            //     .tm_isdst = 0,
            //     .tm_gmtoff = 0,
            //     .tm_zone = @ptrCast([*c]u8, &tmZone),
            // };
            // const tm = c.getdate(@ptrCast([*c]const u8, arg));
            // _ = c.strptime(@ptrCast([*c]const u8, arg), "%Y", &tm);
            // if (c.getdate_err == 0) {
            //     const dateTimeAnswer = try std.fmt.allocPrint(allocator, "Date: {s}", .{tm});
            //     try answers.append(dateTimeAnswer);
            // }

            // var exampleNums = [_]f64{ 1.0, 5.0, 10.0, 20.0 };
            // const statsSummary = runExampleStats(&exampleNums);
            {
                // const statSummaryFmt = "Statistical Summary:\n  {s: >10} {d}\n  {s: >10} {d}\n  {s: >10} {d}\n  {s: >10} {d}\n  {s: >10} {d}";
                // const statsAnswer = try std.fmt.allocPrint(allocator, statSummaryFmt, .{ "Min:", statsSummary.min, "Max:", statsSummary.max, "Count:", statsSummary.count, "Avg/Mean:", statsSummary.mean, "Std. Dev.:", statsSummary.stddev });
                // try answers.append(statsAnswer);
            }
            var finalAnswer: []const u8 = "";
            if (answers.items.len == 0) {
                finalAnswer = try std.fmt.allocPrint(allocator, "Sorry, couldn't do anything with those values.", .{});
            } else {
                finalAnswer = try std.mem.join(allocator, "\n", answers.toOwnedSlice());
            }
            try writer.writeAll(finalAnswer);
            try writer.writeAll("\n");
        },
    }
}

fn findSeparator(arg: []const u8) ?[]const u8 {
    if (std.mem.containsAtLeast(u8, arg, 1, ",")) {
        return ",";
    }

    return null;
}

const PositionalFloat = struct {
    pos: usize,
    num: f64,
};

const PositionalInteger = struct {
    pos: usize,
    num: i64,
};

/// Determine if arg is a single number, collection of numbers, or something else number-like (e.g., a date)
fn normalizeArg(allocator: Allocator, args: ArrayList([]const u8)) !Arg {
    var arg : []const u8 = undefined;
    if (args.items.len == 1) {
        arg = args.items[0];
    } else {
        // TODO Wasteful
        arg = try std.mem.join(allocator, ",", args.items);
    }

    if (findSeparator(arg)) |sep| {
        var numeralIter = std.mem.split(u8, arg, sep);
        var floats = ArrayList(PositionalFloat).init(allocator);
        var ints = ArrayList(PositionalInteger).init(allocator);
        var pos: usize = 0;
        while (numeralIter.next()) |numeral| {
            if (std.fmt.parseInt(i64, numeral, 10)) |num| {
                try ints.append(PositionalInteger{ .pos = pos, .num = num });
            } else |err| switch (err) {
                error.Overflow => {
                    return error.MalformedNumber;
                },
                else => {
                    if (std.fmt.parseFloat(f64, numeral)) |num| {
                        try floats.append(PositionalFloat{ .pos = pos, .num = num });
                    } else |_| {
                        return error.MalformedNumber;
                    }
                },
            }
            pos += 1;
        }

        // One or both are 0-length
        if (floats.items.len == 0 and ints.items.len == 0) {
            return Arg{ .empty = true };
        } else if (floats.items.len == 0) {
            if (ints.items.len == 1) {
                return Arg{ .integer = ints.items[0].num };
            } else {
                var is = ArrayList(i64).init(allocator);
                for (ints.items) |positionalInteger| {
                    try is.append(positionalInteger.num);
                }
                return Arg{ .integers = is };
            }
        } else if (ints.items.len == 0) {
            if (floats.items.len == 1) {
                return Arg{ .float = floats.items[0].num };
            } else {
                var fs = ArrayList(f64).init(allocator);
                for (floats.items) |positionalFloat| {
                    try fs.append(positionalFloat.num);
                }
                return Arg{ .floats = fs };
            }
        }

        // Both arrays have numbers
        var i: usize = 0;
        var fs = ArrayList(f64).init(allocator);
        while (i < ints.items.len) : (i += 1) {
            try fs.append(@intToFloat(f64, ints.items[i].num));
        }
        return Arg{ .floats = fs };
    } else {
        if (std.fmt.parseInt(i64, arg, 10)) |num| {
            return Arg{ .integer = num };
        } else |err| switch (err) {
            error.Overflow => {
                return error.MalformedNumber;
            },
            else => {
                if (std.fmt.parseFloat(f64, arg)) |num| {
                    return Arg{ .float = num };
                } else |_| {
                    return error.MalformedNumber;
                }
            },
        }
    }
}

//
// HTTP
//
fn answerHttp(allocator: Allocator, answers: *ArrayList([]const u8), arg: Arg) !void {
    const statuses = try dataHttp(allocator, arg);
    for (statuses.items) |status| {
        const ans = try std.fmt.allocPrint(allocator, "HTTP: {s} - {s}", .{ status.code, status.description });
        try answers.append(ans);
    }
}

fn dataHttp(allocator: Allocator, arg: Arg) !ArrayList(HttpStatus) {
    var statuses = ArrayList(HttpStatus).init(allocator);
    switch (arg) {
        .integers => |nums| {
            for (nums.items) |num| {
                const maybeStatus = try getHttpStatus(num);
                if (maybeStatus) |status| {
                    try statuses.append(status);
                }
            }
        },
        .integer => |num| {
            const maybeStatus = try getHttpStatus(num);
            if (maybeStatus) |status| {
                try statuses.append(status);
            }
        },
        .float, .floats, .empty => {},
    }
    return statuses;
}

fn getHttpStatus(integer: i64) !?HttpStatus {
    var buf: [3]u8 = undefined;
    const k = try std.fmt.bufPrint(&buf, "{d}", .{integer});
    return httpStatuses.get(k);
}

/// See https://datatracker.ietf.org/doc/html/rfc2616#section-10
/// and https://datatracker.ietf.org/doc/html/rfc7231#page-47
const HttpStatus = struct {
    code: []const u8,
    description: []const u8,
};

const httpStatuses = std.ComptimeStringMap(HttpStatus, .{
    .{ "1xx", .{ .code = "1xx", .description = "Informational" } },
    .{ "100", .{ .code = "100", .description = "Continue" } },
    .{ "101", .{ .code = "101", .description = "Switching Protocols" } },
    .{ "2xx", .{ .code = "2xx", .description = "Successful" } },
    .{ "200", .{ .code = "200", .description = "OK" } },
    .{ "201", .{ .code = "201", .description = "Created" } },
    .{ "202", .{ .code = "202", .description = "Accepted" } },
    .{ "203", .{ .code = "203", .description = "Non-Authoritative Information" } },
    .{ "204", .{ .code = "204", .description = "No Content" } },
    .{ "205", .{ .code = "205", .description = "Reset Content" } },
    .{ "206", .{ .code = "206", .description = "Partial Content" } },
    .{ "3xx", .{ .code = "3xx", .description = "Redirection 3xx" } },
    .{ "300", .{ .code = "300", .description = "Multiple Choices" } },
    .{ "301", .{ .code = "301", .description = "Moved Permanently" } },
    .{ "302", .{ .code = "302", .description = "Found" } },
    .{ "303", .{ .code = "303", .description = "See Other" } },
    .{ "304", .{ .code = "304", .description = "Not Modified" } },
    .{ "305", .{ .code = "305", .description = "Use Proxy" } },
    .{ "306", .{ .code = "306", .description = "(Unused)" } },
    .{ "307", .{ .code = "307", .description = "Temporary Redirect" } },
    .{ "4xx", .{ .code = "4xx", .description = "Client Error 4xx" } },
    .{ "400", .{ .code = "400", .description = "Bad Request" } },
    .{ "401", .{ .code = "401", .description = "Unauthorized" } },
    .{ "402", .{ .code = "402", .description = "Payment Required" } },
    .{ "403", .{ .code = "403", .description = "Forbidden" } },
    .{ "404", .{ .code = "404", .description = "Not Found" } },
    .{ "405", .{ .code = "405", .description = "Method Not Allowed" } },
    .{ "406", .{ .code = "406", .description = "Not Acceptable" } },
    .{ "407", .{ .code = "407", .description = "Proxy Authentication Required" } },
    .{ "408", .{ .code = "408", .description = "Request Timeout" } },
    .{ "409", .{ .code = "409", .description = "Conflict" } },
    .{ "410", .{ .code = "410", .description = "Gone" } },
    .{ "411", .{ .code = "411", .description = "Length Required" } },
    .{ "412", .{ .code = "412", .description = "Precondition Failed" } },
    .{ "413", .{ .code = "413", .description = "Request Entity Too Large" } },
    .{ "414", .{ .code = "414", .description = "Request-URI Too Long" } },
    .{ "415", .{ .code = "415", .description = "Unsupported Media Type" } },
    .{ "416", .{ .code = "416", .description = "Requested Range Not Satisfiable" } },
    .{ "417", .{ .code = "417", .description = "Expectation Failed" } },
    .{ "5xx", .{ .code = "5xx", .description = "Server Error 5xx" } },
    .{ "500", .{ .code = "500", .description = "Internal Server Error" } },
    .{ "501", .{ .code = "501", .description = "Not Implemented" } },
    .{ "502", .{ .code = "502", .description = "Bad Gateway" } },
    .{ "503", .{ .code = "503", .description = "Service Unavailable" } },
    .{ "504", .{ .code = "504", .description = "Gateway Timeout" } },
    .{ "505", .{ .code = "505", .description = "HTTP Version Not Supported" } },
});

const StatsSummary = struct {
    min: f64,
    max: f64,
    count: usize,
    mean: f64,
    stddev: f64,
};

fn runExampleStats(nums: []f64) StatsSummary {
    const cNums = @ptrCast([*c]const f64, nums);

    const mean = c.gsl_stats_mean(cNums, 1, nums.len);
    const stddev = c.gsl_stats_sd_m(cNums, 1, nums.len, mean);
    const largest = c.gsl_stats_max(cNums, 1, nums.len);
    const smallest = c.gsl_stats_min(cNums, 1, nums.len);

    return StatsSummary{
        .min = smallest,
        .max = largest,
        .count = nums.len,
        .mean = mean,
        .stddev = stddev,
    };
}

test "answerHttp" {
    try t.expectEqual(HttpStatus{ .code = "200", .description = "OK" }, getHttpStatus("200").?);
    try t.expectEqual(HttpStatus{ .code = "304", .description = "Not Modified" }, getHttpStatus("304").?);
    try t.expectEqual(HttpStatus{ .code = "5xx", .description = "Server Error 5xx" }, getHttpStatus("5xx").?);
}
